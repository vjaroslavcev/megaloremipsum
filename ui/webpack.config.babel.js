import { resolve as resolvePath } from 'path';
import rules from './config/rules';
import resolve from './config/resolve';
import plugins from './config/plugins';
import devServer from './config/devServer';


export default {
  entry: './ui/src/index.js',
  output: {
    path: resolvePath(__dirname, './ui/dist'),
    filename: 'bundle.js',
    publicPath: '/',
  },
  module: {
    rules,
  },
  resolve,
  plugins,
  devServer,
};
