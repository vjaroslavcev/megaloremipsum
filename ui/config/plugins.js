import HtmlWebpackPlugin from 'html-webpack-plugin';
import { HotModuleReplacementPlugin } from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';


export default [
  new HtmlWebpackPlugin({
    title: 'Auto',
    filename: 'index.html',
  }),
  new HotModuleReplacementPlugin(),
  new ExtractTextPlugin("styles.css"),
];
