export default {
  port: 9000,
  overlay: {
    warnings: true,
    errors: true,
  },
};
