import { extract } from 'extract-text-webpack-plugin';


export default [
  {
    test: /\.js$/,
    loader: 'babel-loader',
    exclude: /node_modules/,
    options: {
      presets: [
        'es2015',
        'stage-0',
      ],
    },
  },
  {
    test: /\.css$/,
    use: extract({
      fallback: "style-loader",
      use: "css-loader"
    })
  }
];
