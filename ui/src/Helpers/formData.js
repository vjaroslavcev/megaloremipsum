export default formId => {
  let data = {}
  const form = document.getElementById(formId)
  const elements = form.querySelectorAll('input[name]')
  elements.forEach(el => data[el.name] = el.value)
  return data
}
