import axios from 'axios';


export default ({ method, route, data }) => {
  return axios({
    method: method || 'GET',
    url: `http://localhost:4000${route}`,
    data,
  })
    .catch(err => console.log(err))
}
