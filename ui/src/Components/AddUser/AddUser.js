import Component from '../Component';
import template from './template';
import ajax from '../../Helpers/ajax';
import formData from '../../Helpers/formData';
import events from '../../events';


export default class extends Component {
  constructor(props) {
    super(props)
    this.data = props.data;
    this.actions = props.actions;
    this.config = {
      template,
      id: 'add-user-popup',
      tagName: 'section',
      appendTo: '.popup-content',
    }
    this.render()
  }

  bindEvents = () => {
    const form = document.querySelector(`#${this.config.id} form`)
    form.addEventListener('submit', this.handleSubmit)
  }

  handleSubmit = e => {
    e.preventDefault()
    const data = formData(this.config.id)
    ajax({
      method: 'POST',
      route: '/users/',
      data,
    }).then(res => {
      this.actions.delete()
      events.publish('userAdded', res.data)
    })
  }

  componentDidMount() {
    this.bindEvents()
  }
}

