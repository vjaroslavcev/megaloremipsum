import html from '../../Helpers/html'


export default () => html(`
  <h2>Add user</h2>
  <form class='form'>
    <input name='firstName' type='text' value='' placeholder='First name'>
    <input name='lastName' type='text' value='' placeholder='Last name'>
    <input name='age' type='text' value='' placeholder='age'>
    <input name='friendsCount' type='text' value='' placeholder='Friends Count'>
    <button type='submit' class='button edit' data-action='save'>Add user</button>
  </form>
`);
