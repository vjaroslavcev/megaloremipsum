import html from '../../Helpers/html'


export default () => html(`
  <h2>Are you sure?</h2>
  <button class='button edit' data-action='delete'>Delete</button>
`);
