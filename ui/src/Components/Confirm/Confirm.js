import Component from '../Component';
import template from './template';
import ajax from '../../Helpers/ajax';
import events from '../../events';


export default class extends Component {
  constructor(props) {
    super(props)
    this.data = props.data;
    this.actions = props.actions;
    this.config = {
      template,
      id: 'confirm-popup',
      appendTo: '.popup-content',
    }
    this.render()
  }

  bindEvents = () => {
    const deleteBtn = document.querySelector('[data-action="delete"]')
    deleteBtn.addEventListener('click', this.handleDelete)
  }

  handleDelete = () => {
    const { data: id } = this;
    ajax({
      method: 'DELETE',
      route: `/users/${id}`
    }).then(res => {
      this.actions.delete()
      events.publish('userRemoved', res.data)
    })
  }

  componentDidMount() {
    this.bindEvents()
  }
}

