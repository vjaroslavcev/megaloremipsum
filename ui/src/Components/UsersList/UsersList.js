import Component from '../Component';
import template from './template';
import UsersListItem from '../UsersListItem/UserListItem';
import ajax from '../../Helpers/ajax';
import events from '../../events';


export default class extends Component {
  constructor(props) {
    super(props)
    this.config = {
      template,
      id: 'users-list',
      appendTo: '#app',
    }
    this.data = [];
    events.subscribe('userAdded', this.renderItem)
    this.getData().then(() => {
      this.render()
    })
  }

  bindEvents = () => {
    const addBtn = document.querySelector('[data-action="add"]')
    addBtn.addEventListener('click', this.handleAdd)
  }

  getData() {
    return ajax({ route: '/users' }).then(res => {
      this.data = [ ...res.data ];
    })
  }

  handleAdd = () => {
    events.publish('showPopup', { name: 'add' })
  }

  renderItem = user => {
    new UsersListItem({
      appendTo: '.table-body',
      data: { ...user },
    })
  }

  componentDidMount() {
    this.bindEvents()
    this.data.forEach(user => this.renderItem(user))
  }
}

