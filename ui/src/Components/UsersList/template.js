import html from '../../Helpers/html'


export default ({ users }) => html(`
  <h1>Users list</h1>
  <table class="table">
    <thead>
      <tr>
        <td>ID</td>
        <td>First name</td>
        <td>Last name</td>
        <td>Age</td>
        <td>Friends</td>
        <td>
          <button class='button add' data-action='add'>Add user</button>
        </td>
      </tr>
    </thead>
    <tbody class='table-body'>
    </tbody>
  </table>
`);
