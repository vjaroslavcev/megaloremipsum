import Component from '../Component';
import Header from '../Header/Header';
import UsersList from '../UsersList/UsersList';
import Popup from '../Popup/Popup';
import EditUser from '../EditUser/EditUser';
import AddUser from '../AddUser/AddUser';
import Confirm from '../Confirm/Confirm';
import events from '../../events';
import getParameter from '../../Helpers/getParameter';


export default class extends Component {
  constructor(props) {
    super(props)
    this.config = {
      id: 'app',
    }
    this.render()
  }


  bindEvents = () => {
    events.subscribe('showPopup', this.showPopup)
  }

  componentDidMount() {
    new Header()
    new UsersList()
    this.bindEvents()
    this.restoreState()
  }

  showPopup = ({ name, id }) => {
    const path = (name ? `/?popup=${name}` : '') + (id ? `&id=${id}`: '');
    history.pushState({}, '', path)
    switch(name) {
      case 'edit':
        return  new Popup({ content: EditUser, data: id })
      case 'add':
        return new Popup({ content: AddUser })
      case 'delete':
        return new Popup({ content: Confirm, data: id })
    }
  }

  restoreState = () => {
    const name = getParameter('popup')
    const id = getParameter('id')
    this.showPopup({ name, id })
  }
}

