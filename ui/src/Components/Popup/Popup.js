import Component from '../Component';
import template from './template';


export default class extends Component {
  constructor(props = {}) {
    super()
    this.config = {
      template,
      id: 'overlay',
      className: 'overlay',
      tagName: 'section',
    };
    this.data = props.data;
    this.actions = props.actions;
    this.content = props.content;
    this.render()
  }

  bindEvents = () => {
    const cancelBtn= document.querySelector('[data-action="cancel"]');
    cancelBtn.addEventListener('click', this.handleCancel)
    window.addEventListener('keydown', this.handleKeyDown)
    window.addEventListener('click', this.handleClick)
  }

  handleCancel = () => {
    this.removePopup()
  }

  handleKeyDown = ({ key }) => {
    if ( key === 'Escape' || key === 'x') {
      this.removePopup()
    }
  }

  handleClick = (e) => {
    if (e.target.className === this.config.className) {
      this.removePopup()
    }
  }

  removePopup = () => {
    const el = document.getElementById(this.config.id);
    el.classList.add('leave')
    // TODO add cross browser animation helper
    el.addEventListener('webkitAnimationEnd', this.remove.bind(this))
  }

  renderPopupContent() {
    const { content: Content, data, actions } = this
    new Content({
      data,
      actions: {
        ...actions,
        delete: this.removePopup,
      }
    })
  }

  componentDidMount() {
    this.renderPopupContent()
    const scrollWidth = window.innerWidth - document.documentElement.clientWidth;
    document.body.style.paddingRight = `${scrollWidth}px`
    document.body.classList.add('noScroll')
    this.bindEvents()
  }

  componentWillUnmount() {
    document.body.classList.remove('noScroll');
    document.body.removeAttribute('style')
    window.removeEventListener('keydown', this.handleKeyDown)
    window.removeEventListener('click', this.handleClick)
    history.pushState(null, '', '/')
  }
}

