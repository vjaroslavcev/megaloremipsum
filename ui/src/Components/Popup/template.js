import html from '../../Helpers/html'


export default () => html(`
  <div class="popup">
    <button class='button x-btn' data-action='cancel'>X</button>
    <div class='popup-content'></div>
  </div>
`);
