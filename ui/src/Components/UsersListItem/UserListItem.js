import Component from '../Component';
import template from './template';
import events from '../../events';


export default class extends Component {
  constructor(props) {
    super(props)
    this.data = props.data;
    this.config = {
      template,
      id: `row-${this.data.id}`,
      className: 'row',
      tagName: 'tr',
      appendTo: props.appendTo,
    };
    this.sbur = events.subscribe('userRemoved', this.userRemoved)
    this.sbuc = events.subscribe('userChanged', this.userChanged)
    this.render()
  }

  bindEvents = () => {
    const id = this.config.id;
    const editBtn = document.querySelector(`#${id} .edit`)
    const deleteBtn = document.querySelector(`#${id} .delete`)
    editBtn.addEventListener('click', this.handleEdit)
    deleteBtn.addEventListener('click', this.handleDelete)
  }

  handleDelete = () => {
    events.publish('showPopup', {
      name: 'delete',
      id: this.data.id,
    })
  }

  handleEdit = () => {
    events.publish('showPopup', {
      name: 'edit',
      id: this.data.id,
    })
  }

  userRemoved = ({ id }) => {
    if (id == this.data.id) {
      this.remove()
    }
  }

  userChanged = data => {
    if (data.id == this.data.id) {
      this.data = data
      this.reRender()
      this.bindEvents()
    }
  }

  componentDidMount() {
    this.bindEvents()
  }

  componentWillUnmount(){
    this.sbur.remove()
    this.sbuc.remove()
  }
}

