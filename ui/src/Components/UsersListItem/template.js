import html from '../../Helpers/html'


export default ({ id, firstName, lastName, age, friendsCount }) => html(`
  <td>${id}</td>
  <td>${firstName}</td>
  <td>${lastName}</td>
  <td>${age}</td>
  <td>${friendsCount}</td>
  <td>
    <button class='button edit'>Edit</button>
    <button class='button delete'>Delete</button>
  </td>
`);
