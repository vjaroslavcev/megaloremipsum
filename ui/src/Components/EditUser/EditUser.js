import Component from '../Component';
import template from './template';
import ajax from '../../Helpers/ajax';
import formData from '../../Helpers/formData';
import events from '../../events';


export default class extends Component {
  constructor(props) {
    super(props)
    this.data = props.data;
    this.actions = props.actions;
    this.config = {
      template,
      id: 'edit-user-popup',
      tagName: 'section',
      appendTo: '.popup-content',
    }
    ajax({ route: `/users/${this.data}` }).then(res => {
      this.data = { ...res.data };
      this.render()
    })
  }

  bindEvents = () => {
    const form = document.querySelector(`#${this.config.id} form`)
    form.addEventListener('submit', this.handleSubmit)
  }

  handleSubmit = e => {
    e.preventDefault()
    const data = formData(this.config.id)
    ajax({
      method: 'PUT',
      route: `/users/${this.data.id}`,
      data,
    }).then(res => {
      this.actions.delete()
      events.publish('userChanged', res.data)
    })
  }

  componentDidMount() {
    this.bindEvents()
  }
}

