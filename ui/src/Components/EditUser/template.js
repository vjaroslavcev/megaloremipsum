import html from '../../Helpers/html'


export default ({ id, firstName, lastName, age, friendsCount }) => html(`
  <h2>Edit user</h2>
  <form class='form'>
    <input disabled name='id' type='text' value=${id}>
    <input name='firstName' type='text' value=${firstName}>
    <input name='lastName' type='text' value=${lastName}>
    <input disabled name='age' type='text' value=${age}>
    <input disabled name='friendsCount' type='text' value=${friendsCount}>
    <button type='submit' class='button edit' data-action='save'>Save</button>
  </form>
`);
