import Component from '../Component';
import template from './template';

export default class extends Component {
  constructor(props) {
    super(props)
    this.config = {
      template,
      id: 'header',
      tagName: 'header',
      appendTo: '#app',
    }
    this.render()
  }
}

