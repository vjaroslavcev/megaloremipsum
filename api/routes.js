import {
  getAllUsers,
  addUser,
  getUser,
  updateUser,
  removeUser
} from './controllers';


export default app => {
  app.route('/users')
    .get(getAllUsers)
    .post(addUser)

  app.route('/users/:userId')
    .get(getUser)
    .put(updateUser)
    .delete(removeUser)
}
