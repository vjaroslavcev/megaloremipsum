export const randomNumber = (max = 100, min = 1) => (
  Math.floor(Math.random() * ((max - min) + 1)) + min
);


const generateDataItem = index => {
  const id = index + 1
  return {
    id,
    firstName: `FirstName-${id}`,
    lastName: `LastName-${id}`,
    age: randomNumber(10, 80),
    friendsCount: randomNumber(),
  }
};


const generateArray = size => (
  [...new Array(size)].map((item, index) => generateDataItem(index))
);

export default generateArray(200);
