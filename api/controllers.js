import store from './data';


// mutable store
let Store = store



export const getAllUsers = (req, res) => {
  return res.json(Store);
};


export const addUser = (req, res ) => {
  const id = Store[Store.length - 1].id + 1
  console.log(id)
  const user = {
    ...req.body,
    id,
  }
  // mutating store
  Store.push(user)
  return res.json(user)
}


export const getUser = (req, res) => {
  const user = Store.find(obj => Number(obj.id) === Number(req.params.userId));
  return res.json(user);
};


export const updateUser = (req , res) => {
  const index = Store.findIndex(obj => Number(obj.id) === Number(req.body.id));
  // mutating store
  Store[index] = { ...req.body }
  return res.json(req.body)
};


export const removeUser = (req, res) => {
  // mutating store
  Store = Store.filter(obj => Number(obj.id) !== Number(req.params.userId));
  return res.json({ id: req.params.userId});
}
